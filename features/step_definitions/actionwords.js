exports.Actionwords = {
    theUserInputsAnEmailAddress: function () {

    },
    theUserInputsAPassword: function () {

    },
    theEmailAddressIsVerified: function () {

    },
    aNewAccountIsCreated: function () {

    },
    theUserLogsInToTheAccount: function () {

    },
    userChoosesToChangeDisplayName: function () {

    },
    userInputsTheirNewDisplayName: function () {

    },
    theSystemVerifiesIt: function () {

    },
    theDisplayNameWillChange: function () {

    },
    theUserInputsTheirEmailAddress: function () {

    },
    choosesToResetTheirPassword: function () {

    },
    theUserInputsTheirNewPassword: function () {

    },
    theSystemVerifiesTheNewPassword: function () {

    },
    thePasswordWillBeChanged: function () {

    },
    theUserScansAPosterUsingTheirSmartphone: function () {

    },
    theSystemAnalyzesTheScan: function () {

    },
    theReviewsRatingsAndTitleForThePosterWillShowUp: function () {

    },
    theUserIsLoggedInToTheirAccount: function () {

    },
    theyHaveScannedAPoster: function () {

    },
    theyCanRateTheInformation: function () {

    },
    theUserIsLoggedIntoTheirAccount: function () {

    },
    theyScanAMoviePoster: function () {

    },
    theyCanGiveAReviewAndRateTheMovie: function () {

    },
    theUserIsLoggedIn: function () {

    },
    theyAutomaticallySeeTheNumberOfContributionsTheyHaveMade: function () {

    }
};