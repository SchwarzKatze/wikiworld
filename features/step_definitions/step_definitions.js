module.exports = function () {
    this.Before(function (scenario) {
        this.actionwords = Object.create(require('./actionwords.js').Actionwords);
    });

    this.After(function (scenario) {
        this.actionwords = null;
    });


    this.Given(/^the user inputs an email address$/, function (callback) {
        this.actionwords.theUserInputsAnEmailAddress();
        callback();
    });

    this.Given(/^the user inputs a password$/, function (callback) {
        this.actionwords.theUserInputsAPassword();
        callback();
    });

    this.When(/^the email address is verified$/, function (callback) {
        this.actionwords.theEmailAddressIsVerified();
        callback();
    });

    this.Then(/^a new account is created$/, function (callback) {
        this.actionwords.aNewAccountIsCreated();
        callback();
    });

    this.Given(/^the user logs in to the account$/, function (callback) {
        this.actionwords.theUserLogsInToTheAccount();
        callback();
    });

    this.Given(/^user chooses to change display name$/, function (callback) {
        this.actionwords.userChoosesToChangeDisplayName();
        callback();
    });

    this.Given(/^user inputs their new display name$/, function (callback) {
        this.actionwords.userInputsTheirNewDisplayName();
        callback();
    });

    this.When(/^the system verifies it$/, function (callback) {
        this.actionwords.theSystemVerifiesIt();
        callback();
    });

    this.Then(/^the display name will change$/, function (callback) {
        this.actionwords.theDisplayNameWillChange();
        callback();
    });

    this.Given(/^the user inputs their email address$/, function (callback) {
        this.actionwords.theUserInputsTheirEmailAddress();
        callback();
    });

    this.Given(/^chooses to reset their password$/, function (callback) {
        this.actionwords.choosesToResetTheirPassword();
        callback();
    });

    this.Given(/^the user inputs their new password$/, function (callback) {
        this.actionwords.theUserInputsTheirNewPassword();
        callback();
    });

    this.When(/^the system verifies the new password$/, function (callback) {
        this.actionwords.theSystemVerifiesTheNewPassword();
        callback();
    });

    this.Then(/^the password will be changed$/, function (callback) {
        this.actionwords.thePasswordWillBeChanged();
        callback();
    });

    this.Given(/^the user scans a poster using their smartphone$/, function (callback) {
        this.actionwords.theUserScansAPosterUsingTheirSmartphone();
        callback();
    });

    this.When(/^the system analyzes the scan$/, function (callback) {
        this.actionwords.theSystemAnalyzesTheScan();
        callback();
    });

    this.Then(/^the reviews, ratings and title for the poster will show up$/, function (callback) {
        this.actionwords.theReviewsRatingsAndTitleForThePosterWillShowUp();
        callback();
    });

    this.Given(/^the user is logged in to their account$/, function (callback) {
        this.actionwords.theUserIsLoggedInToTheirAccount();
        callback();
    });

    this.Given(/^they have scanned a poster$/, function (callback) {
        this.actionwords.theyHaveScannedAPoster();
        callback();
    });

    this.Then(/^they can rate the information$/, function (callback) {
        this.actionwords.theyCanRateTheInformation();
        callback();
    });

    this.Given(/^the user is logged into their account$/, function (callback) {
        this.actionwords.theUserIsLoggedIntoTheirAccount();
        callback();
    });

    this.Given(/^they scan a movie poster$/, function (callback) {
        this.actionwords.theyScanAMoviePoster();
        callback();
    });

    this.Then(/^they can give a review and rate the movie$/, function (callback) {
        this.actionwords.theyCanGiveAReviewAndRateTheMovie();
        callback();
    });

    this.Given(/^the user is logged in$/, function (callback) {
        this.actionwords.theUserIsLoggedIn();
        callback();
    });

    this.Then(/^they automatically see the number of contributions they have made$/, function (callback) {
        this.actionwords.theyAutomaticallySeeTheNumberOfContributionsTheyHaveMade();
        callback();
    });
}
