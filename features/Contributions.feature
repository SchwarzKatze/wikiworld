Feature: Contributions


  Scenario: Review Poster
    Given the user is logged into their account
    And they scan a movie poster
    Then they can give a review and rate the movie

  Scenario: Contribution Number
    Given the user is logged in
    Then they automatically see the number of contributions they have made
