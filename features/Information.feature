Feature: Information


  Scenario: Scanning a Poster
    Given the user scans a poster using their smartphone
    When the system analyzes the scan
    Then the reviews, ratings and title for the poster will show up

  Scenario: Upvote Information
    Given the user is logged in to their account
    And they have scanned a poster
    Then they can rate the information
