Feature: Manage Account


  Scenario: Create Account
    # A new user creates an account to use the app
    Given the user inputs an email address
    And the user inputs a password
    When the email address is verified
    Then a new account is created

  Scenario: Change Display Name
    Given the user logs in to the account
    And user chooses to change display name
    And user inputs their new display name
    When the system verifies it
    Then the display name will change

  Scenario: Reset Password
    Given the user inputs their email address
    And chooses to reset their password
    And the user inputs their new password
    When the system verifies the new password
    Then the password will be changed
